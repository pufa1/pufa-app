package it.app.pufa;

import com.fasterxml.jackson.databind.ObjectMapper;
import it.app.pufa.controllers.auth.AuthController;
import it.app.pufa.data_access.models.Sezione;
import it.app.pufa.data_access.models.auth.AppUser;
import it.app.pufa.data_access.models.auth.ERole;
import it.app.pufa.data_access.models.auth.Role;
import it.app.pufa.repositories.SezioneRepository;
import it.app.pufa.repositories.SottoSezioneRepository;
import it.app.pufa.repositories.auth.RoleRepository;
import it.app.pufa.repositories.auth.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

@SpringBootApplication
public class PufaApplication implements CommandLineRunner {

    private PathMatchingResourcePatternResolver resolver;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private SottoSezioneRepository sottoSezioneRepository;

    @Autowired
    private SezioneRepository sezioneRepository;

    @Autowired
    PasswordEncoder encoder;


    @Value("classpath:menu/*.json")
    private Resource[] modelli;

    @Value("classpath:utenza/admin.json")
    private Resource mockUtente;

    public static void main(String[] args) {
        SpringApplication.run(PufaApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {

        inserisciRuoli();
        initializeSezAndSottoSez();
        insertMockUtente();
    }

    /**
     * @throws IOException
     */
    private void initializeSezAndSottoSez() throws IOException {
        if (sezioneRepository.findAll().size() == 0) {
            for (Resource modello : modelli) {

                Sezione sezione = objectMapper.readValue(modello.getInputStream(), Sezione.class);
                sezioneRepository.save(sezione);
            }
        }
    }

    @Autowired
    private AuthController authcont;

    @Autowired
    private RoleRepository roleRepository;

    private void inserisciRuoli() {
        Role admin = new Role(ERole.ADMIN);
        roleRepository.save(admin);
        Role user = new Role(ERole.USER);
        roleRepository.save(user);
    }

    private void insertMockUtente() throws IOException {
    	if(userRepository.findAll().size() == 0) {
            AppUser user = new AppUser();
            user.setId(1L);
            user.setUsername("nike");
            user.setPassword(encoder.encode("nike"));
            Set<Role> roles = new HashSet<>();
            roles.add(new Role(ERole.ADMIN));
            roles.add(new Role(ERole.USER));
            user.setRoles(roles);
            userRepository.save(user);
        }
    }
}
