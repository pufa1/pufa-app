package it.app.pufa.repositories;

import it.app.pufa.data_access.models.Articolo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ArticoloRepository extends JpaRepository<Articolo, Long> {
}
