package it.app.pufa.data_access.models;

import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
public class Sezione {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    private String nome;

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    private List<SottoSezione> sottoSezioni;
}
