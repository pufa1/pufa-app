package it.app.pufa.controllers.gestione_sezioni;

import it.app.pufa.data_access.models.Articolo;
import it.app.pufa.data_access.models.Sezione;
import it.app.pufa.data_access.models.SottoSezione;
import it.app.pufa.repositories.SezioneRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/pufa/sezioni/")
public class SezioneController {

    @Autowired
    private SezioneRepository sezioneRepository;

    @GetMapping("recuperasezioni")
    public ResponseEntity<List<Sezione>> getAllSezioni() {

        try {

            List<Sezione> sezioni = sezioneRepository.findAll();
            for(Sezione sezione: sezioni) {
                for(SottoSezione sottoSezione: sezione.getSottoSezioni()) {
                    for(Articolo articolo: sottoSezione.getArticoli()) {
                        articolo.setImage(null);
                    }
                }
            }
            return new ResponseEntity<>(sezioni, HttpStatus.ACCEPTED);
        }
        catch (Exception e) {

        }
        return null;
    }

    @GetMapping("recuperasezione/{id}")
    public ResponseEntity<Sezione> getAllSottoSezioni(@PathVariable(value = "id") long id) {

        try {
            Optional<Sezione> sezione = sezioneRepository.findById(id);
            if(sezione.isPresent()) {
                return new ResponseEntity<Sezione>(sezione.get(), HttpStatus.OK);
            }
        }
        catch (Exception e) {
            throw e;
        }
        return null;
    }
}
