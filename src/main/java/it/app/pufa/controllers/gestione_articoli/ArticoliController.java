package it.app.pufa.controllers.gestione_articoli;


import io.swagger.annotations.ApiOperation;
import it.app.pufa.data_access.models.Articolo;
import it.app.pufa.gestione_articoli.services_impl.ArticoloServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;


@RestController
@RequestMapping("/pufa/articoli/")
public class ArticoliController {

    Logger logger = LoggerFactory.getLogger(ArticoliController.class);
    @Autowired
    private ArticoloServiceImpl articoloService;

    /**
     * @param articolo
     * @param idsezione
     * @param idsottosezione
     * @return
     */
    @PostMapping("addArticolo/{idsezione}/{idsottosezione}")
    @ApiOperation(value = "aggiunta articolo", response = Articolo.class)
    public ResponseEntity addArticolo(@RequestParam("file") MultipartFile file, @ModelAttribute Articolo articolo, @PathVariable(value = "idsezione") Long idsezione, @PathVariable(value = "idsottosezione") Long idsottosezione) throws Exception {

        try {
            if(!file.isEmpty()) {
                articolo.setImage(file.getBytes());
            }
            return ResponseEntity
                    .status(HttpStatus.OK)
                    .body(articoloService.addArticolo(articolo, idsezione, idsottosezione));
        } catch (
                Exception e) {

            logger.info("inserimento articolo non avvenuto: " + e.getMessage());
            return ResponseEntity
                    .status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(e.getMessage());
        }
    }

    /**
     * @param articolo
     * @param idArticoloOld
     * @return
     */
    @PostMapping("updateArticolo/{idarticolo}")
    @ApiOperation(value = "aggiornamento di un articolo", response = Articolo.class)
    public ResponseEntity updateArticolo(@ModelAttribute Articolo articolo, @PathVariable(value = "idarticolo") Long idArticoloOld, @RequestParam("file") MultipartFile file) {

        try {
            if(!file.isEmpty()) {
                articolo.setImage(file.getBytes());
            }
            return ResponseEntity
                    .status(HttpStatus.ACCEPTED)
                    .body(articoloService.updateArticolo(idArticoloOld, articolo));
        } catch (Exception e) {

            logger.info("aggiornamento articolo non avvenuto: " + e.getMessage());
            return ResponseEntity
                    .status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(e.getMessage());
        }
    }

    /**
     *
     * @param idArticolo
     * @return
     * @throws Exception
     */
    @GetMapping("getArticolo/{idarticolo}")
    @ApiOperation(value = "recupero articolo", response = Articolo.class)
    public ResponseEntity getArticolo(@PathVariable(value = "idarticolo") Long idArticolo) throws Exception {

        try {
            return ResponseEntity
                    .status(HttpStatus.OK)
                    .body(articoloService.getArticolo(idArticolo));
        }
        catch (Exception e) {
            logger.info("recupero dell'articolo non avvenuto con successo: " + e.getMessage());
            return ResponseEntity
                    .status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(e.getMessage());
        }
    }

    /**
     *
     * @return
     * @throws Exception
     */
    @GetMapping("getAllArticoli")
    @ApiOperation(value = "aggiunta tutti articoli", response =Articolo.class, responseContainer = "List")
    public ResponseEntity getArticoli() throws Exception {

        try {
            return ResponseEntity
                    .status(HttpStatus.OK)
                    .body(articoloService.getAllArticoli());
        }
        catch (Exception e) {
            logger.info("recupero degli articoli non avvenuto con successo: " + e.getMessage());
            return ResponseEntity
                    .status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(e.getMessage());
        }
    }

    /**
     *
     * @param idSottoSezione
     * @return
     */
    @GetMapping("getArticoli/{id}")
    public ResponseEntity getArticoliBySottoSezione(@PathVariable(value = "id") Long idSottoSezione) {

        try {
            return ResponseEntity
                    .status(HttpStatus.OK)
                    .body(articoloService.getArticoloByIdSottoSezione(idSottoSezione));
        }
        catch (Exception e) {
            logger.info("recupero degli articoli non avvenuto con successo: " + e.getMessage());
            return ResponseEntity
                    .status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(e.getMessage());
        }
    }


    /**
     *
     * @param idArticolo
     * @return
     * @throws Exception
     */
    @GetMapping("deleteArticolo/{idarticolo}")
    public ResponseEntity deleteArticolo(@PathVariable(value = "idarticolo") Long idArticolo) throws Exception {

        try {
            articoloService.deleteArticolo(idArticolo);
            return ResponseEntity
                    .status(HttpStatus.OK)
                    .body("articolo cancellato con successo");
        }
        catch (Exception e) {
            logger.info("cancellazione articolo non avvenuta con successo: " + e.getMessage());
            return ResponseEntity
                    .status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(e.getMessage());
        }
    }


}
