package it.app.pufa.repositories;

import it.app.pufa.data_access.models.SottoSezione;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SottoSezioneRepository extends JpaRepository<SottoSezione, Long> {
}
