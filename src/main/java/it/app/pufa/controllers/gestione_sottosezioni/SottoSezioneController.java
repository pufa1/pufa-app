package it.app.pufa.controllers.gestione_sottosezioni;

import it.app.pufa.data_access.models.Sezione;
import it.app.pufa.data_access.models.SottoSezione;
import it.app.pufa.repositories.SezioneRepository;
import it.app.pufa.repositories.SottoSezioneRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/pufa/sottosezione/")
public class SottoSezioneController {

    @Autowired
    private SezioneRepository sezioneRepository;

    @Autowired
    private SottoSezioneRepository sottoSezioneRepository;

    @GetMapping("recuperasottosezioni/{id}")
    public ResponseEntity<SottoSezione> getAllSottoSezioni(@PathVariable(value = "id") long id) {

        try {
            Optional<SottoSezione> sottoSezione = sottoSezioneRepository.findById(id);
            if(sottoSezione.isPresent()) {
                return new ResponseEntity<>(sottoSezione.get(), HttpStatus.OK);
            }
        }
        catch (Exception e) {
            throw e;
        }
        return null;
    }
}
