package it.app.pufa.data_access.models.auth;


import it.app.pufa.data_access.models.AnagraficaUser;
import lombok.Data;

import java.util.List;

@Data
public class JwtResponse {

    private String jwt;

    private Long id;

    private String username;

    private AnagraficaUser anagrafica;

    private List<String> roles;

    public JwtResponse(String jwt, Long id, String username, AnagraficaUser anagrafica, List<String> roles) {
        this.jwt = jwt;
        this.id = id;
        this.username = username;
        this.anagrafica = anagrafica;
        this.roles = roles;
    }
}
