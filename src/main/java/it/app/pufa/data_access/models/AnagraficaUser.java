package it.app.pufa.data_access.models;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Entity
@Data
public class AnagraficaUser {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    private String nome;

    private String cognome;

    private String email;

    private String recapito_telefonico;

    private Date dataNascita;

    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Indirizzo indirizzo;
}
