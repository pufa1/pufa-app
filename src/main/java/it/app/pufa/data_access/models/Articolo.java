package it.app.pufa.data_access.models;

import com.fasterxml.jackson.annotation.JsonSetter;
import com.sun.org.apache.xerces.internal.impl.dv.util.Base64;
import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.*;
import java.io.UnsupportedEncodingException;
import java.sql.Blob;

@Data
@Entity
public class Articolo {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    private String nome;

    private String descrizione;

    private Double prezzo;

    @Lob
    private byte[] image;

}
