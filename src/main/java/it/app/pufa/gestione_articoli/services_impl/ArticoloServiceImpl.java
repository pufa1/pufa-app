package it.app.pufa.gestione_articoli.services_impl;

import it.app.pufa.controllers.gestione_articoli.ArticoliController;
import it.app.pufa.data_access.models.Articolo;
import it.app.pufa.data_access.models.Sezione;
import it.app.pufa.data_access.models.SottoSezione;
import it.app.pufa.repositories.ArticoloRepository;
import it.app.pufa.repositories.SezioneRepository;
import it.app.pufa.repositories.SottoSezioneRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ArticoloServiceImpl {

    Logger logger = LoggerFactory.getLogger(ArticoloServiceImpl.class);

    @Autowired
    private ArticoloRepository articoloRepository;

    @Autowired
    private SezioneRepository sezioneRepository;

    @Autowired
    private SottoSezioneRepository sottoSezioneRepository;

    /**
     * Aggiunta di un nuovo articolo relativo alla sezione
     * e sottoSezione di appartenenza
     * @param articolo
     * @param idSezione
     * @param idSottoSezione
     * @return
     */
    public Articolo addArticolo(Articolo articolo, long idSezione, long idSottoSezione) throws Exception {

        try {
            Articolo articoloSaved = articoloRepository.save(articolo);
            Optional<Sezione> sezione = sezioneRepository.findById(idSezione);
            if(sezione.isPresent()) {

                for(SottoSezione sottoSezionef: sezione.get().getSottoSezioni()) {

                    if(sottoSezionef.getId() == idSottoSezione) {

                        sottoSezionef.getArticoli().add(articoloSaved);
                        sezioneRepository.save(sezione.get());
                        break;
                    }
                }
            }
            return articoloSaved;
        }
        catch (Exception e) {
            throw new Exception("salvataggio articolo non avvenuto");
        }
    }

    /**
     *
     * @param idArticoloToUpdate
     * @param articoloNew
     * @return
     * @throws Exception
     */
    public Articolo updateArticolo(long idArticoloToUpdate, Articolo articoloNew) throws Exception {

        try {
            Optional<Articolo> articoloOldOpt = articoloRepository.findById(idArticoloToUpdate);
            if(articoloOldOpt.isPresent()) {

                Articolo articoloOld = articoloOldOpt.get();
                articoloOld = assembleArticolo(articoloOld, articoloNew);
                return articoloRepository.save(articoloOld);
            }
        }
        catch (Exception e) {
            logger.info("ERROR AGGIORNAMENTO --->  " + e.getMessage());
            throw new Exception("Aggiornamento dell'articolo non avvenuto con successo");
        }
        return null;
    }

    /**
     * Recupero di un articolo tramite il relativo id
     * @param idArticolo
     * @return
     * @throws Exception
     */
    public Articolo getArticolo(long idArticolo) throws Exception {

        try {
            return articoloRepository.findById(idArticolo).get();
        }
        catch (Exception e) {
            throw new Exception("Errore nel recupero dell'articolo");
        }
    }

    /**
     *
     * @return
     * @throws Exception
     */
    public List<Articolo> getAllArticoli() throws Exception {

        try {
            return articoloRepository.findAll();
        }
        catch (Exception e) {
            throw new Exception("Errore nel recupero degli articoli");
        }
    }

    /**
     * Recupero degli articoli in base alla sottosezionee
     * @param idSottosezione
     * @return
     * @throws Exception
     */
    public List<Articolo> getArticoloByIdSottoSezione(long idSottosezione) throws Exception {
        try {
            Optional<SottoSezione> articoliFiltered = sottoSezioneRepository.findById(idSottosezione);
            if(articoliFiltered.isPresent()) {
                return articoliFiltered.get().getArticoli();
            }
        }
        catch (Exception e) {
            throw new Exception("Errore nel recupero degli articoli");
        }
        return null;
    }

    /**
     *
     * @param idArticolo
     * @throws Exception
     */
    public void deleteArticolo(long idArticolo) throws Exception{

        try {
            Optional<Articolo> articoloToDelete = articoloRepository.findById(idArticolo);
            if(articoloToDelete.isPresent()) {
                articoloRepository.delete(articoloToDelete.get());
            }
            else {
                throw new Exception("Articolo da eliminare non trovato");
            }
        }
        catch (Exception e) {
            logger.info("errore nella cancellazione dell'articolo: " + e.getMessage());
            throw e;
        }
    }


    //-------------------------------METODI UTILITY-----------------------------------------------------------------
    /**
     * Metodo privato di utility per aggiornare un articolo mantenendo il vecchio id
     * @param articoloOld
     * @param articoloNew
     * @return
     */
    private Articolo assembleArticolo(Articolo articoloOld, Articolo articoloNew) {

        articoloOld.setNome(articoloNew.getNome());
        articoloOld.setDescrizione(articoloNew.getDescrizione());
        articoloOld.setPrezzo(articoloNew.getPrezzo());
        if(articoloNew.getImage()!= null) {
            articoloOld.setImage(articoloNew.getImage());
        }
        return articoloOld;
    }
}
