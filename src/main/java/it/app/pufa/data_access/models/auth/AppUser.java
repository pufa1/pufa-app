package it.app.pufa.data_access.models.auth;

import it.app.pufa.data_access.models.AnagraficaUser;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Data
public class AppUser {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String username;

    private String password;

    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private AnagraficaUser anagrafica;

    @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Set<Role> roles = new HashSet<>();



    public AppUser(String username, AnagraficaUser anagrafica, String encode) {
        this.username = username;
        this.anagrafica = anagrafica;
        this.password = encode;
    }

    public AppUser() {
    }

    public AppUser(Long id, String username, AnagraficaUser anagrafica, String password, Set<Role> roles) {
        this.id = id;
        this.username = username;
        this.anagrafica = anagrafica;
        this.password = password;
        this.roles = roles;
    }
}
