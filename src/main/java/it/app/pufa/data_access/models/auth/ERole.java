package it.app.pufa.data_access.models.auth;

public enum ERole {

    ADMIN, USER;

}
