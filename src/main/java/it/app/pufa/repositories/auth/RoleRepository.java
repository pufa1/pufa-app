package it.app.pufa.repositories.auth;

import it.app.pufa.data_access.models.auth.ERole;
import it.app.pufa.data_access.models.auth.Role;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface RoleRepository extends JpaRepository<Role, Long> {
    Optional<Role> findByName(ERole name);
}