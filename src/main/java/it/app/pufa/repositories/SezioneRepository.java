package it.app.pufa.repositories;

import it.app.pufa.data_access.models.Sezione;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SezioneRepository extends JpaRepository<Sezione, Long> {
}
